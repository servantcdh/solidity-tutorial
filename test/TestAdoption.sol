pragma solidity ^0.4.17-0.5.0;

/****************************

    Truffle 프레임워크로 빌드 및 마이그레이션된 스마트 계약을 테스트하기 위한 로직.

*****************************/

// 다음의 두 줄은 truffle 디렉토리가 아닌 전역 Truffle 파일을 import하는 것.
import "truffle/Assert.sol"; // 테스트에서 사용할 다양한 assertion을 제공.
import "truffle/DeployedAddresses.sol"; // 배포된 스마트 계약의 주소를 가져옴.

import "../contracts/Adoption.sol"; // 테스트하고자 하는 스마트 계약

contract TestAdoption {
    Adoption adoption = Adoption(DeployedAddresses.Adoption());
    uint expectedPetId = 8;
    address expectedAddress = this; // this : 지금 보고 있는 스마트 계약.
    
    // adoption 메소드 테스트
    function testUserCanAdoptPet() public {
        uint returnedPetId = adoption.adopt(expectedPetId);
        Assert.equal(returnedPetId, expectedPetId, "아이디 8번 펫이 기록되어 있습니다."); // equal가 false일 경우 세번째 인자의 메세지가 표시되면서 예외 발생
    }

    // 입양한 Pet의 소유자 가져오기 테스트
    function testGetAdopterAddressByPetId() public {
        address adopter = adoption.adopters(expectedPetId);
        Assert.equal(adopter, expectedAddress, "아이디 8번 펫의 소유자가 기록되어 있습니다.");
    }

    function testGetAdopterAddressByPetIdInArray() public {
        address[16] memory adopters = adoption.getAdopters();
        Assert.equal(adopters[expectedPetId], expectedAddress, "아이디 8번 펫의 소유자가 기록되어 있습니다.");
    }
}