App = {
  web3Provider: null,
  contracts: {}, // 내가 작성한 스마트 계약 아티팩트가 담기게 될 객체

  init: async function() {
    // Load pets.
    $.getJSON('../pets.json', function(data) {
      var petsRow = $('#petsRow');
      var petTemplate = $('#petTemplate');

      for (i = 0; i < data.length; i ++) {
        petTemplate.find('.panel-title').text(data[i].name);
        petTemplate.find('img').attr('src', data[i].picture);
        petTemplate.find('.pet-breed').text(data[i].breed);
        petTemplate.find('.pet-age').text(data[i].age);
        petTemplate.find('.pet-location').text(data[i].location);
        petTemplate.find('.btn-adopt').attr('data-id', data[i].id);

        petsRow.append(petTemplate.html());
      }
    });

    return await App.initWeb3();
  },

  initWeb3: async function() { // web3 : 이더리움 블록체인 네트워크에 접속해 ABI를 다룰 수 있도록 하는 라이브러리
    if (typeof web3 !== 'undefined') { // 이미 활성화된 web3 인스턴스가 있는지 체크
      App.web3Provider = web3.currentProvider;
    } else { // 없으면 가니쉬(127.0.0.1:7545)에서 가져옴.
      App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7545');
    }
    web3 = new Web3(App.web3Provider);

    return App.initContract();
  },

  initContract: function() {
    $.getJSON('Adoption.json', function(data) {
      // 컴파일된 스마트 계약 아티팩트(build/contracts/<contract>.json)를 가져와 TruffleContract 함수를 통해 초기화.
      var AdoptionArtifact = data;
      App.contracts.Adoption = TruffleContract(AdoptionArtifact);

      // 프로바이더 설정
      App.contracts.Adoption.setProvider(App.web3Provider);

      return App.markAdopted();
    });

    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '.btn-adopt', App.handleAdopt);
  },

  markAdopted: function() {
    var adoptionInstance;

    App.contracts.Adoption.deployed().then(function(instance) { // 미리 초기화해놓은 스마트 계약 아티팩트를 deployed 함수를 통해 인스턴스 생성.
      adoptionInstance = instance;

      return adoptionInstance.getAdopters.call(); // call 메소드를 사용하면 트랜잭션 통신 필요없이 블록체인에서 데이터를 가져올 수 있다.
                                                  // 다시 말해 수수료없이 데이터를 가져올 수 있다는 의미. view 키워드로 작성된 읽기 전용 메소드 한정.
    }).then(function(adopters) {
      for (var i = 0; i < adopters.length; i++) {
        if (adopters[i] !== '0x0000000000000000000000000000000000000000') { // 펫의 소유자 계정이 존재한다면 비활성화.
          $('.panel-pet').eq(i).find('button').text('Success').attr('disabled', true);
        }
      }
    }).catch(function(err) {
      console.log(err.message);
    });
  },

  handleAdopt: function(event) {
    event.preventDefault();

    var petId = parseInt($(event.target).data('id')); // 마크업단 이벤트로 petId를 가져옴.

    var adoptionInstance;

    web3.eth.getAccounts(function(error, accounts) { // 가니쉬에서 이더리움 계정을 10개 가져옴.
      if (error) {
        console.log(error);
      }

      var account = accounts[0]; // 첫번째 계정을 사용.

      App.contracts.Adoption.deployed().then(function(instance) {
        adoptionInstance = instance;

        // 블록체인 트랜잭션 통신으로 스마트 계약 내에 작성했던 입양 메소드 실행.
        return adoptionInstance.adopt(petId, {from: account}); // 이 객체의 from이 스마트 계약의 msg.sender가 됨.
      }).then(function(result){
        return App.markAdopted();
      }).catch(function(err) {
        console.log(err.message);
      })
    })
  }

};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
